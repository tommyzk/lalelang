import { Injectable } from '@angular/core';

import { Item } from '../../models/item';
import { AuthService } from '../../service/AuthService';
import { LelangService } from '../../service/LelangService';

@Injectable()
export class Items {
  items: Item[] = [];
  allLelang: Item[];
  lelang = [];

  defaultItem: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "Burt is a Bear.",
  };


  constructor(public authSvc: AuthService, public lelangSvc: LelangService) {
    let items = [
      {
        "name": "Burt Bear",
        "profilePic": "assets/img/speakers/bear.jpg",
        "about": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat dui eu mauris imperdiet, id malesuada nisl laoreet. Ut bibendum semper dui ut consectetur. Aliquam ultricies neque elementum, ornare lectus ut, ultrices massa. Proin pellentesque lobortis ipsum quis scelerisque. In hac habitasse platea dictumst. Etiam ac consequat enim. Sed sit amet turpis quam. Morbi pretium vitae risus non pellentesque. Sed imperdiet dapibus gravida. Nunc vulputate gravida vol"
      },
      {
        "name": "Charlie Cheetah",
        "profilePic": "assets/img/speakers/cheetah.jpg",
        "about": "Charlie is a Cheetah."
      },
      {
        "name": "Donald Duck",
        "profilePic": "assets/img/speakers/duck.jpg",
        "about": "Donald is a Duck."
      },
      {
        "name": "Eva Eagle",
        "profilePic": "assets/img/speakers/eagle.jpg",
        "about": "Eva is an Eagle."
      },
      {
        "name": "Ellie Elephant",
        "profilePic": "assets/img/speakers/elephant.jpg",
        "about": "Ellie is an Elephant."
      },
      {
        "name": "Molly Mouse",
        "profilePic": "assets/img/speakers/mouse.jpg",
        "about": "Molly is a Mouse."
      },
      {
        "name": "Paul Puppy",
        "profilePic": "assets/img/speakers/puppy.jpg",
        "about": "Paul is a Puppy."
      }
    ];

    console.log("item ", items);

    this.lelang = []; 
    var lelangRef = this.lelangSvc.getLelang();

    lelangRef.on('value', personSnapshot => {
      this.allLelang = this.objectToArray(personSnapshot.val());

      Object.keys(this.allLelang).forEach( key => {
        var detailItems = {
          pelelang: this.allLelang[key].pelelang,
          namaBarang: this.allLelang[key].namaBarang,
          kode: this.allLelang[key].kode,
          tanggalLelang: this.allLelang[key].tanggalLelang,
          jamLelang: this.allLelang[key].jamLelang,
          imageUrl: this.allLelang[key].imageUrl,
          deskripsiBarang: this.allLelang[key].deskripsiBarang
        }

        this.lelang.push(detailItems);
      });
    });

    for (let item of this.lelang) {
      console.log("add: " + item);
      this.items.push(new Item(item));
    }
  }

  private objectToArray(obj) {
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };

  query(params?: any) {

    console.log(params);
    console.log(this.items);
    console.log(this.lelang);
    if (!params) {
      return this.items;
    }

    return this.lelang.filter((item) => {
      for (let key in params) {
        console.log(key);
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
