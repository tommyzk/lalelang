import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'convertTime',
})

export class ConvertTimePipe implements PipeTransform {
  transform(time) {

    let second = Math.floor((time % (1000 * 60)) / 1000);
    let minute = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
    let hour = Math.floor((time % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let day = Math.floor(time / (1000 * 60 * 60 * 24));

    let timeString = day + "D " + hour + "H " + minute + "M " + second + "S";
    return timeString; 
  }
}
