import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { FirstRunPage, MainPage } from '../pages/pages';
import { Settings } from '../providers/providers';
import firebase from 'firebase';
import { App } from 'ionic-angular/components/app/app';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

@Component({
  template: `<ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = FirstRunPage;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Tutorial', component: 'TutorialPage' },
    { title: 'Welcome', component: 'WelcomePage' },
    { title: 'Tabs', component: 'TabsPage' },
    { title: 'Cards', component: 'CardsPage' },
    { title: 'Content', component: 'ContentPage' },
    { title: 'Login', component: 'LoginPage' },
    { title: 'Signup', component: 'SignupPage' },
    { title: 'Master Detail', component: 'ListMasterPage' },
    { title: 'Menu', component: 'MenuPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Search', component: 'SearchPage' },
    { title: 'StartLelang', component: 'StartLelangPage' },
    { title: 'Favorite', component: 'FavoritePage' },
    { title: 'Profile', component: 'ProfilePage' },
    { title: 'Verify', component: 'VerifyPage' },
    { title: 'Change Password', component : 'ChangepasswordPage' }
  ]

  constructor(private translate: TranslateService, 
    platform: Platform, 
    settings: Settings, 
    public modalCtrl: ModalController,
    private config: Config, 
    private statusBar: StatusBar, 
    private splashScreen: SplashScreen,
    private app: App) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      
     // this.splashScreen.hide();
    });
    this.initTranslate();

    firebase.initializeApp({
      apiKey: "AIzaSyCM-uf45O5SCkpCaQ9O3m31_aFoh-qFFWo",
      authDomain: "lalelang-32be4.firebaseapp.com",
      databaseURL: "https://lalelang-32be4.firebaseio.com",
      projectId: "lalelang-32be4",
      storageBucket: "lalelang-32be4.appspot.com",
      messagingSenderId: "123061483386"
    })

    firebase.auth().onAuthStateChanged( user => {
      if (!user) {
          this.rootPage = this.pages[1].component
        
      } else { 
        if (user.emailVerified != true){
          this.rootPage = this.pages[14].component
        } else {
          this.rootPage = this.pages[2].component
        }
      }
    });
    
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    //this.translate.addLangs(['en','id','ja']);
    this.translate.setDefaultLang('id');
    this.translate.setDefaultLang('ja');
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }


    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
