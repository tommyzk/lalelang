import firebase from 'firebase';
import { Injectable } from '@angular/core';
import { AuthService } from './AuthService';
import { storage } from 'firebase';

@Injectable()
export class LelangService {
    constructor(public authService:AuthService) {
        
    }

    addLelang(namaBarang: string, hargaBarang: string, kondisiBarang: string, tanggalSelesai: string, deskripsiBarang: string, image: any): Promise<any> {
        
        const uid = this.authService.getCurrentUser().uid;
        const namaPelelang = this.authService.getCurrentUser().displayName
        const imageUrl = "fotoLelangan/" + uid + this.randomString(15)
        const pictures = storage().ref(imageUrl)
        pictures.putString(image, 'data_url')
        var date = new Date();
        var kodeBarang = 'Lel' + date.getFullYear() + ('0' + (date.getMonth() + 1)).slice(-2) + ('0' + date.getDate()).slice(-2) 
        + date.getHours() + date.getMinutes() + date.getSeconds() + this.randomString(15);
        var dateLelang = date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) 
        var timeLelang = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

        var postData = {
            kode: kodeBarang, 
            pelelang: uid, 
            namaPelelang: namaPelelang,
            namaBarang: namaBarang, 
            hargaBarang: hargaBarang, 
            kondisiBarang: kondisiBarang, 
            deskripsiBarang: deskripsiBarang, 
            imageUrl: image,
            tanggalSelesai: tanggalSelesai,
            tanggalLelang: dateLelang,
            jamLelang: timeLelang
          };

        var updates = {};

        updates['/lelangan/' + kodeBarang] = postData;
        updates['/userProfile/' + uid + '/startLelang/' + kodeBarang] = postData;

        return firebase
            .database()
            .ref()
            .update(updates);

    }

    getLelang(){
        const lelanganRef: firebase.database.Reference = firebase.database().ref(`/lelangan/`);
        return lelanganRef;
    }

    getLelangById(uid){
        const lelanganRef: firebase.database.Reference = firebase.database().ref(`/lelangan/` + uid + '/');
        return lelanganRef;
    }

    getFavorites(){
        const uid = this.authService.getCurrentUser().uid;
        const lelanganRef: firebase.database.Reference = firebase.database().ref(`/userProfile/` + uid + '/favoriteLelang/');
        return lelanganRef;
    }
    
    getHistoryBid(){
        const uid = this.authService.getCurrentUser().uid;
        const lelanganRef: firebase.database.Reference = firebase.database().ref(`/userProfile/` + uid + '/startBid/')
        return lelanganRef;
    }
    
    getHistoryLelang(){
        const uid = this.authService.getCurrentUser().uid;
        const lelanganRef: firebase.database.Reference = firebase.database().ref(`/userProfile/` + uid + '/startLelang/')
        return lelanganRef;
    }

    checkFavorite(kode){
        const uid = this.authService.getCurrentUser().uid;
        return firebase.database().ref(`/userProfile/` + uid + '/favoriteLelang/' + kode + "/")
    }

    favorite(kode: string, pelelang: string){
        const uid = this.authService.getCurrentUser().uid
        return firebase
            .database()
            .ref('/userProfile/')
            .child(uid)
            .child('favoriteLelang')
            .child(kode)
            .set({ kode: kode, pelelang: pelelang })
    }

    unfavorite(kode: string){
        const uid = this.authService.getCurrentUser().uid
        /*return firebase
        .database()
        .ref('/userProfile/' + uid + '/favoriteLelang/')
        .child(kode).remove()*/
        
        var updates = {}
        updates['/userProfile/' + uid + '/favoriteLelang/' + kode ] = null;
      
        return firebase.database().ref().update(updates);
    }

    randomString(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    newBidLelang(nominalBid, kodeBarang) {
        // A post entry.
        const uid = this.authService.getCurrentUser().uid;
        var date = new Date();
        var kodeBid = 'Bid' + date.getFullYear() + ('0' + (date.getMonth() + 1)).slice(-2) + ('0' + date.getDate()).slice(-2) 
        + date.getHours() + date.getMinutes() + date.getSeconds() + this.randomString(15);
        var dateBid = date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) 
        var timeBid = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

        var postData = {
            namaBider: this.authService.getCurrentUser().displayName,
            nominalBid: nominalBid,
            tanggalBid: dateBid,
            jamBid: timeBid

        };
        var nomiData = {
            kodeBarang: kodeBarang,
            nominalBid: nominalBid,
            tanggalBid: dateBid,
            jamBid: timeBid
        }
      
        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        updates['/lelangan/' + kodeBarang + '/bider/' + kodeBid] = postData;
        updates['/userProfile/' + uid + '/startBid/' + kodeBid] = nomiData;
        updates['/lelangan/' + kodeBarang + '/hargaBarang' ] = nominalBid;
        updates['/lelangan/' + kodeBarang + '/bidBy' ] = uid;
      
        return firebase.database().ref().update(updates);
      }
}