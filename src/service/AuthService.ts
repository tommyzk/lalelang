import firebase from 'firebase';

export class AuthService {

    curr: any;
    date = new Date().toISOString().substring(0,10);

    signup(nama: string, telp: string, address: string, email: string, password: string): Promise<any> {
        return firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then( newUser => {
            
            firebase
            .auth()
            .currentUser
            .sendEmailVerification()

            firebase
            .database()
            .ref('/userProfile')
            .child(newUser.uid)
            .set({
                fullName: nama, 
                email: email,
                telp: telp,
                address: address,
                saldo: 0,
                registerDate: this.date
             });
        });
    }

    resentVerEmail(){
        return firebase.auth().currentUser.sendEmailVerification()
    }

    signin(email: string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email,password)
    }

    getCurrentUser(){
        return firebase.auth().currentUser;
    }

    logout() {
        return firebase.auth().signOut();
    }

    updateProfile(name: string, email: string, telp: string, address: string, image: string, uid: string) {

        var updates = {};
        updates['/userProfile/' + uid + '/fullName' ] = name;
        updates['/userProfile/' + uid + '/telp' ] = telp;
        updates['/userProfile/' + uid + '/address' ] = address;
        updates['/userProfile/' + uid + '/imageUrl' ] = image;
      
        return firebase.database().ref().update(updates);
    }

    topUp(uid: string, nominal: number) {
        var updates = {};
        updates['/userProfile/' + uid + '/saldo' ] = nominal;
        
        return firebase.database().ref().update(updates);
    }

    getUserById(uid) {
        const userRef: firebase.database.Reference = firebase.database().ref(`/userProfile/` + uid + '/');
        return userRef;
    }

    getUserByEmail(email) {
        const userRef = firebase.database()
            .ref()
            .child('userProfile')
            .orderByChild('email')
            .equalTo(email);
        return userRef;
    }

    changePassword(newPassword: string) {
        const user = firebase.auth().currentUser;
        return user.updatePassword(newPassword);
    }

}