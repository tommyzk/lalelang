import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, Loading, LoadingController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';
import { AuthService } from '../../service/AuthService';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };
  

  // Our translated text strings
  public loading: Loading;
  private loginErrorString: string;
  private loginSuccessString: string;
  

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public loadingCtrl: LoadingController,
    private authService: AuthService) {

      this.translateService.get("LOGIN_ERROR").subscribe((value) => {
        this.loginErrorString = value;
      })

  }

  // Attempt to login in through our User service
  doLogin() {

    this.authService.signin(this.account.email,this.account.password).then(
    (result) => {
      this.loading.dismiss().then( () => {
        this.translateService.get("LoginSuccess").subscribe((value) => {
          this.loginSuccessString = value;
        })
        
        let toast = this.toastCtrl.create({
          message: this.loginSuccessString,
          duration: 3000,
          position: 'bottom'
        });
        toast.present();

        //this.navCtrl.push(MainPage);
      })
    })
    .catch((error) => {
      this.loading.dismiss().then( () => {
      

        let toast = this.toastCtrl.create({
          message: this.loginErrorString,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      })
    });
    this.loading = this.loadingCtrl.create();
    this.loading.present();
  }
}
