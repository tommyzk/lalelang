import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, AlertController, ToastController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';
import { LelangService } from '../../service/LelangService';
import { AuthService } from '../../service/AuthService';
import { Http } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Item[];
  favoriteItems: Item[];
  img: any;
  userName: string;
  detailItem = [];
  detailFavorites = [];
  data: any = {};

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, public lelangSvc: LelangService,
    public authSvc: AuthService, public alertCtrl: AlertController, public toastCtrl: ToastController,
    public http: Http) {
    
    this.http = http;
  }

  sendEmailTes(){
    var link = 'http://192.168.0.6/apisms.php';
    var myData = JSON.stringify({username: "franco"});
    
    this.http.post(link, myData)
    .subscribe(data => {
    this.data.response = data["_body"]; //https://stackoverflow.com/questions/39574305/property-body-does-not-exist-on-type-response
    }, error => {
    });
  }

  ionViewWillEnter() {
    this.detailItem = [];
    var lelanganRef = this.lelangSvc.getLelang()
    var favoriteRef = this.lelangSvc.getFavorites()

    favoriteRef.on('value', personSnapshot => {
      this.detailFavorites = [];
      this.favoriteItems = this.objectToArray(personSnapshot.val());
    })

    lelanganRef.on('value', personSnapshot => {
      this.detailItem = [];
      this.currentItems = this.objectToArray(personSnapshot.val());

      Object.keys(this.currentItems).forEach( key => {
        var userProfile = this.authSvc.getUserById(this.currentItems[key].pelelang);
        userProfile.on('value', personSnapshot => {
          var temp

          if(personSnapshot.val().imageUrl) {
            temp = personSnapshot.val().imageUrl;
          } else {
            temp = `http://www.racialjusticenetwork.co.uk/wp-content/uploads/2016/12/default-profile-picture-1.png`;
          }

          var isFav = false;

          var data = this.lelangSvc.checkFavorite(this.currentItems[key].kode).once('value', function(snapshot){
            
            if(snapshot.val() == null){
              isFav = false;
            }else {
              isFav = true;
            }
          })

          var detailItems = {
            fullName: personSnapshot.val().fullName,
            profilPic: temp,
            pelelang: this.currentItems[key].pelelang,
            namaBarang: this.currentItems[key].namaBarang,
            kode: this.currentItems[key].kode,
            tanggalLelang: this.currentItems[key].tanggalLelang,
            jamLelang: this.currentItems[key].jamLelang,
            imageUrl: this.currentItems[key].imageUrl,
            deskripsiBarang: this.currentItems[key].deskripsiBarang,
            isFav: isFav
          }
  
          this.detailItem.push(detailItems)
        });
      });
    });
  }

  objectToArray(obj) {
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };


  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

  confirmFavorite(item: any) {
    let alert = this.alertCtrl.create({
      title: 'Favorite',
      message: 'Do you want to add this to your favorites?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.favorite(item);
          }
        }
      ]
    });
    alert.present();
  }

  confirmUnfavorite(item: any) {
    let alert = this.alertCtrl.create({
      title: 'Favorite',
      message: 'Do you want to remove this from your favorites?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.unfavorite(item);
          }
        }
      ]
    });
    alert.present();
  }

  favorite(item: any){
    this.lelangSvc.favorite(item.kode, item.pelelang)
      .then( () => {
        let toast = this.toastCtrl.create({
          message: "Success add to favorites",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        })
        item.isFav = true;
      //this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  unfavorite(item: any){
    this.lelangSvc.unfavorite(item.kode)
      .then( () => {
        let toast = this.toastCtrl.create({
          message: "Success remove from favorites",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        })
        item.isFav = false;
  }
}
