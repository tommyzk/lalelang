import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TopUpPage } from './top-up';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TopUpPage,
    
  ],
  imports: [
    IonicPageModule.forChild(TopUpPage),
    TranslateModule.forChild()
  ],
})
export class TopUpPageModule {}
