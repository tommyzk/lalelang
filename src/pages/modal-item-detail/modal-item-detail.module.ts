import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ModalItemDetailPage } from './modal-item-detail';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ModalItemDetailPage,
    
  ],
  imports: [
    IonicPageModule.forChild(ModalItemDetailPage),
    TranslateModule.forChild(),
    PipesModule 
  ],
})
export class ModalItemDetailPageModule {}
