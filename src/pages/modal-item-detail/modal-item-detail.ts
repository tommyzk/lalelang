import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Loading } from 'ionic-angular';
import { LelangService } from '../../service/LelangService';
import { AuthService } from '../../service/AuthService';
import { TranslateService } from '@ngx-translate/core';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Observable } from 'rxjs/Rx';
import * as moment from 'moment';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { CurrencyPipe } from '@angular/common';

/**
 * Generated class for the ModalItemDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-item-detail',
  templateUrl: 'modal-item-detail.html',
})
export class ModalItemDetailPage {
  lastBid: number;
  title: string;
  titleFailed: string;
  titleSuccess: string;
  titleFailed2: string;
  message: string;
  bidAmmount: number;
  item: any;
  fullName: string;
  saldo: any;
  date: any;
  tick: any;
  add: any;


  public loading: Loading;
  currency;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController, 
    public alertCtrl: AlertController,
    public lelangSvc: LelangService,
    public authSvc: AuthService,
    public translateService: TranslateService,
    public loadingCtrl: LoadingController,
    public currencyPipe: CurrencyPipe
  ) {
    this.currency = currencyPipe;
    var itemTemp = navParams.get('item')
    var itemId = itemTemp.kode
    var temp = lelangSvc.getLelangById(itemId)
    temp.on('value', personSnapshot => {
      this.item = personSnapshot.val()

      if(this.item.bidBy) {
        this.authSvc.getUserById(this.item.bidBy).on('value', personSnapshot => {
          this.fullName = personSnapshot.val().fullName;
          
        });
      }
    });

    var uidBider = authSvc.getCurrentUser().uid;
    this.authSvc.getUserById(uidBider).on('value', personSnapshot => {
      this.saldo = parseInt(personSnapshot.val().saldo)
      
    });

    this.add = moment(this.item.tanggalSelesai+" "+this.item.jamLelang, "YYYY-MM-DD hh:mm:ss");
    Observable.interval(1000).subscribe( 
      t => {
        this.date = moment();
        this.tick = this.add - this.date;
      }
    );
    
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ModalItemDetailPage');
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  bid() {
    this.loading = this.loadingCtrl.create();
    this.loading.present();

    this.titleSuccess = "Bid ERROR!"
    this.message = "BID ERROR!";
    
    if (this.bidAmmount == null){
      this.bidAmmount = 0;
    }
    var hargaBarang:Number = parseInt(this.item.hargaBarang)
    if(this.bidAmmount > this.saldo) {
      

    let alert = this.alertCtrl.create({
      title: "Bid Failed!",
      message: "Bid must Higher than your balance",
      buttons: [{
        text: 'OK',
        role: 'cancel',
        handler: data => {
          this.bidAmmount = null
        }
      }]
    });
      
      this.loading.dismiss()
        alert.present();
    
      
      
      
    }
    else if(this.bidAmmount < hargaBarang && this.bidAmmount < this.saldo) {
      

    let alert = this.alertCtrl.create({
      title: "Bid Failed!",
      message: "Bid must Higher than current Price",
      buttons: [{
        text: 'OK',
        role: 'cancel',
        handler: data => {
          this.bidAmmount = null
        }
      }]
    });

      this.loading.dismiss()
        alert.present();
      
      
      
    } else if ( this.bidAmmount > hargaBarang && this.bidAmmount < this.saldo) {
      
      this.lelangSvc.newBidLelang(this.bidAmmount, this.item.kode )
      .then( () => {

    let alert = this.alertCtrl.create({
      title: "Bid Success!",
      message: "Success to bid this item",
      buttons: [{
        text: 'OK',
        role: 'cancel',
        handler: data => {
          this.bidAmmount = null
        }
      }]
    });
        
        this.loading.dismiss()
          alert.present();
        })
        
      
    } else {

    let alert = this.alertCtrl.create({
      title: "Bid Failed!",
      message: "Something wrong",
      buttons: [{
        text: 'OK',
        role: 'cancel',
        handler: data => {
          this.bidAmmount = null
        }
      }]
    });
      this.loading.dismiss()
        alert.present();
      
      
      
    }

    
    
    
  }
  

  

}
