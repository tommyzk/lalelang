import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartLelangPage } from './start-lelang';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    StartLelangPage,
  ],
  imports: [
    IonicPageModule.forChild(StartLelangPage),
    TranslateModule.forChild()
  ],
})
export class StartLelangPageModule {}
