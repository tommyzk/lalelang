import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, ActionSheetController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { LelangService } from '../../service/LelangService';


/**
 * Generated class for the StartLelangPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start-lelang',
  templateUrl: 'start-lelang.html',
})
export class StartLelangPage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;
  item: any;
  form: FormGroup;
  image: any;
  namaBarang: string; hargaBarang: string;
  kondisiBarang: string; tanggalSelesai: string; deskripsiBarang: string;

  constructor(public navCtrl: NavController, 
              public viewCtrl: ViewController, formBuilder: FormBuilder, public camera: Camera,
              public lelangSvc: LelangService, public toastCtrl: ToastController, 
              public actionSheetCtrl: ActionSheetController) {
    this.form = formBuilder.group({
      profilePic: [''],
      namaBarang: ['', Validators.required],
      hargaBarang: ['', Validators.required],
      kondisiBarang: ['', Validators.required],
      deskripsiBarang: ['', Validators.required],
      tanggalSelesai: ['', Validators.required]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {

  }

  getPicture() {
    try{
      const cameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 512,
        targetHeight: 512,
        correctOrientation: true
      }

      const pickerOptions = {
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 512,
        targetHeight: 512,
        correctOrientation: true
      }

      let actionSheet = this.actionSheetCtrl.create({
        title: 'Modify your album',
        buttons: [
          {
            text: 'Camera',
            handler: () => {
              //   //const result = 
                this.camera.getPicture(cameraOptions)
                .then((result) => {
                  this.image = `data:image/jpeg;base64,${result}`
                  this.form.patchValue({ 'profilePic': this.image });
                  
                }, (err) => {
                    //alert('Unable to take photo');
                    console.log("Unable to take photo");
                })
            }
          },{
            text: 'Album',
            handler: () => {
              this.camera.getPicture(pickerOptions)
                .then((result) => {
                  this.image = `data:image/jpeg;base64,${result}`
                  this.form.patchValue({ 'profilePic': this.image });
                }, (err) => {
                  //alert('Unable to take photo');
                  console.log("Unable to take photo");
                })
            }
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
            }
          }
        ]
      });
      
      actionSheet.present();
    
    }
    catch(e){
      console.error(e);
    }
    
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  createItem() {
    if (this.form.valid) {
      this.namaBarang = this.form.value.namaBarang;
      this.hargaBarang = this.form.value.hargaBarang;
      this.kondisiBarang = this.form.value.kondisiBarang;
      this.tanggalSelesai = this.form.value.tanggalSelesai;
      this.deskripsiBarang = this.form.value.deskripsiBarang;
      this.lelangSvc.addLelang(this.namaBarang, this.hargaBarang, this.kondisiBarang, this.tanggalSelesai, this.deskripsiBarang, this.image)
      .then( () => {

        this.form.reset();

        let toast = this.toastCtrl.create({
          message: "Success Add Lelang",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        })
    }
  }
}
