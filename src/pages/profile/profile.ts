import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { AuthService } from '../../service/AuthService';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  @ViewChild('fileInput') fileInput;
  isReadyToSave: boolean;
  item: any;
  form: FormGroup;
  displayName: any;
  name: any;
  email: any;
  telp: any;
  address: any;
  uid: any;
  image: any;

  constructor(
    public navCtrl: NavController, 
    public viewCtrl: ViewController, 
    formBuilder: FormBuilder, 
    public camera: Camera,
    public authSrvc: AuthService,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.form = formBuilder.group({
      profilePic: [''],
      namaUser: ['', Validators.required],
      AddressUser: ['', Validators.required],
      EmailUser: ['', Validators.required],
      PhoneUser: ['', Validators.required]
    });

    this.email = this.authSrvc.getCurrentUser().email;

    this.authSrvc.getUserByEmail(this.authSrvc.getCurrentUser().email).on('value', personSnapshot => {
      this.item = this.objectToArray(personSnapshot.val());

      this.name = this.item[0].fullName;
      this.email = this.item[0].email;
      this.telp = this.item[0].telp;
      this.address = this.item[0].address;
      this.image = this.item[0].imageUrl;
    });

    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });

    this.uid = this.authSrvc.getCurrentUser().uid;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  objectToArray(obj) {
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };

  getPicture() {
    try{
      const cameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 512,
        targetHeight: 512,
        correctOrientation: true
      }

      const pickerOptions = {
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 512,
        targetHeight: 512,
        correctOrientation: true
      }

      let actionSheet = this.actionSheetCtrl.create({
        title: 'Modify your album',
        buttons: [
          {
            text: 'Camera',
            handler: () => {
              //   //const result = 
                this.camera.getPicture(cameraOptions)
                .then((result) => {
                  this.image = `data:image/jpeg;base64,${result}`
                  this.form.patchValue({ 'profilePic': this.image });
                  
                }, (err) => {
                })
            }
          },{
            text: 'Album',
            handler: () => {
              this.camera.getPicture(pickerOptions)
                .then((result) => {
                  this.image = `data:image/jpeg;base64,${result}`
                  this.form.patchValue({ 'profilePic': this.image });
                }, (err) => {
                })
            }
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
            }
          }
        ]
      });
      
      actionSheet.present();
    
    }
    catch(e){
      console.error(e);
    }
    
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  editProfile() {
    if (this.form.valid) { 
      this.name = this.form.value.namaUser;
      this.email = this.form.value.EmailUser;
      this.telp = this.form.value.PhoneUser;
      this.address = this.form.value.AddressUser;
      this.authSrvc.updateProfile(this.name, this.email, this.telp, this.address, this.image, this.uid).then(() => {
        this.navCtrl.pop();
      });
    }
  }

}

