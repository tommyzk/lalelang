import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, Loading, LoadingController } from 'ionic-angular';
import { User } from '../../providers/providers';
import { MainPage } from '../pages';
import { AuthService } from '../../service/AuthService';
import { LoginPage } from '../login/login';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  
  // account: { name: string, email: string, password: string } = {
  //   name: 'Test Human',
  //   email: 'test@example.com',
  //   password: 'asdasd'
  // };

  account: { name: string , telp: string, address: string, email: string, password: string } = {
    name: '', telp: '', address: '', email: '', password: ''
  };

  // Our translated text strings
  public loading: Loading;
  private signupErrorString: string;
  private signupSuccessString: string;
  public form: FormGroup;
  pages = { title: 'Tabs', component: 'TabsPage' }

  constructor(
    public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public loadingCtrl: LoadingController,
    public authSvc: AuthService,
    public modalCtrl: ModalController,
    private authService: AuthService,
    public formBuilder: FormBuilder
  ) {

    this.form = formBuilder.group({
      fullName: ['', Validators.required],
      telp: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      retype: ['', Validators.required]
    }, {validator: SignupPage.passwordsMatch});

  }

  static passwordsMatch(fg: FormGroup): {[err: string]: any} {
    let password = fg.get('password');
    let retype = fg.get('retype');
    let rv: {[error: string]: any} = {};
    if ((password.touched || retype.touched) && password.value !== retype.value) {
      rv['passwordMismatch'] = true;
    }
    return rv;
  }
  
  doSignup() {
  
    this.account.name = this.form.value.fullName;
    this.account.telp = this.form.value.telp;
    this.account.address = this.form.value.address;
    this.account.email = this.form.value.email;
    this.account.password = this.form.value.password;
    this.authService.signup(this.account.name, 
      this.account.telp,
      this.account.address, 
      this.account.email, 
      this.account.password).then(
      (result) => {

        this.loading.dismiss().then( () => {
          this.translateService.get("SignupSuccess").subscribe((value) => {
            this.signupSuccessString = value;
          })
          
          let toast = this.toastCtrl.create({
            message: this.signupSuccessString,
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          //this.navCtrl.setRoot(this.pages.component), {}, {animate: true, direction: 'forward'};
          //this.navCtrl.push('LoginPage');
      })
    })
    .catch((error) => {
      this.loading.dismiss().then( () => {
        this.translateService.get(error.code).subscribe((value) => {
          this.signupErrorString = value;
        })

        let toast = this.toastCtrl.create({
          message: this.signupErrorString,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      })
    });
    
    this.loading = this.loadingCtrl.create();
    this.loading.present();
  }

}
