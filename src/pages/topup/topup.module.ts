import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TopupPage } from './topup';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TopupPage,
  ],
  imports: [
    IonicPageModule.forChild(TopupPage),
    TranslateModule.forChild()
  ],
})
export class TopupPageModule {}
