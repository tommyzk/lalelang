import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../service/AuthService';

/**
 * Generated class for the TopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-topup',
  templateUrl: 'topup.html',
})
export class TopupPage {
  form: FormGroup;
  nominal: number;
  uid: any;
  temp: number;

  constructor(public authSvc: AuthService, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      nominal: ['', Validators.required],
    });
  }

  topUp() {
      var total;
      this.nominal = parseInt(this.form.value.nominal);
      this.uid = this.authSvc.getCurrentUser().uid;

      this.authSvc.getUserById(this.uid).on('value', personSnapshot => {
        this.temp = parseInt(personSnapshot.val().saldo);
      });

      total = this.temp + this.nominal;

      this.authSvc.topUp(this.uid, total).then( () => {
        this.navCtrl.pop();
      });
    
  }

}
