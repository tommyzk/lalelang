import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the PopoverShareSocmedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover-share-socmed',
  templateUrl: 'popover-share-socmed.html',
})
export class PopoverShareSocmedPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController, 
    public social: SocialSharing) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverShareSocmedPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  facebook() {
    this.social.shareViaFacebook("testing").then( () => {
    }).catch( () => {
    });
  }

}
