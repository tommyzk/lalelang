import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverShareSocmedPage } from './popover-share-socmed';

@NgModule({
  declarations: [
    PopoverShareSocmedPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverShareSocmedPage),
  ],
})
export class PopoverShareSocmedPageModule {}
