import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../service/AuthService';

/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {

  form: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public formBuilder: FormBuilder,
    public authSvc: AuthService,
    public toastCtrl: ToastController
  ) {
    this.form = this.formBuilder.group({
      oldPass: ['', Validators.required],
      newPass: ['', Validators.required],
      retypePass: ['', Validators.required]
    }, {validator: ChangepasswordPage.passwordsMatch});
  }

  static passwordsMatch(fg: FormGroup): {[err: string]: any} {
    let newPass = fg.get('newPass');
    let retypePass = fg.get('retypePass');
    let rv: {[error: string]: any} = {};
    if ((newPass.touched || retypePass.touched) && newPass.value !== retypePass.value) {
      rv['passwordMismatch'] = true;
    }
    return rv;
  }

  changePassword() {

    this.authSvc.signin(this.authSvc.getCurrentUser().email, this.form.value.oldPass).then((result) => {
        this.authSvc.changePassword(this.form.value.newPass).then(() => {
          this.navCtrl.pop();
        });
      }
    ).catch((error) => {
      
      let toast = this.toastCtrl.create({
        message: 'failed',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      console.error(error)
      
    });    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }

}
