import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Settings } from '../../providers/providers';
import { AuthService } from '../../service/AuthService';
import { auth } from 'firebase';
import { WelcomePage } from '../welcome/welcome';
import { App } from 'ionic-angular/components/app/app';
import { MyApp } from '../../app/app.component';
import { TutorialPage } from '../tutorial/tutorial';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CurrencyPipe } from '@angular/common';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  // Our local settings object
  options: any;
  language: String;
  settingsReady = false;

  form: FormGroup;

  profileSettings = {
    page: 'profile',
    pageTitleKey: 'SETTINGS_PAGE_PROFILE'
  };

  page: string = 'main';
  pageTitleKey: string = 'SETTINGS_TITLE';
  pageTitle: string;
  loggedIn: string;
  email: string;
  fullName: string;
  telp: string;
  image: any;
  saldo: any;
  item: any[];
  subSettings: any = SettingsPage;

  constructor(public navCtrl: NavController,
    public settings: Settings,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    public translate: TranslateService,
    public authSvc: AuthService,
    public modalCtrl: ModalController,
    private app: App, 
    public currencyPipe: CurrencyPipe) {
      this.language = translate.currentLang;

  }

  ionViewDidLoad() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});
  }

  ionViewWillEnter() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});

    this.page = this.navParams.get('page') || this.page;
    this.pageTitleKey = this.navParams.get('pageTitleKey') || this.pageTitleKey;

    this.translate.get(this.pageTitleKey).subscribe((res) => {
      this.pageTitle = res;
    })

    if(this.authSvc.getCurrentUser())
    {
      this.loggedIn = this.authSvc.getCurrentUser().displayName;
      this.email = this.authSvc.getCurrentUser().email;

      this.authSvc.getUserByEmail(this.email).on('value', personSnapshot => {
        this.item = this.objectToArray(personSnapshot.val());

        this.fullName = this.item[0].fullName;
        this.email = this.item[0].email;
        this.telp = this.item[0].telp;
        this.saldo = this.item[0].saldo;
        
        var temp;
        if(this.item[0].imageUrl) {
          temp = this.item[0].imageUrl;
        } else {
          temp = `http://www.racialjusticenetwork.co.uk/wp-content/uploads/2016/12/default-profile-picture-1.png`;
        }

        this.image = temp;
      });
      
    } else {
      this.loggedIn = "logged out";
    }
  }

  objectToArray(obj) {
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };


  logout(){

    //this.navCtrl.pop();
    this.authSvc.logout().then(()=>{
    })
    .catch((error) => {
        console.error(error)
    });
    //this.navCtrl.popToRoot();
    //this.navCtrl.setRoot(WelcomePage, {}, {animate: true, direction: 'back'});
    this.app.getRootNav().setRoot('TutorialPage', {animate: true, direction: 'back'});
    //this.navCtrl.setRoot(TutorialPage, {animate: true, direction: 'back'});
    
  }

  changeLang(value:string) {
    this.translate.use(value);
  }
  
  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  goToEditProfile(){
    //let modal = this.modalCtrl.create("ProfilePage")
    //modal.present()
    this.navCtrl.push('ProfilePage', {
      item: this.item
    });
  }

  topUp(){
    this.navCtrl.push('TopupPage');
  }

  withdraw(){
    this.navCtrl.push('TopUpPage');
  }
  
  changePassword(){
    this.navCtrl.push('ChangepasswordPage');
  }

  history(){
    this.navCtrl.push('HistoryPage');
  }
}
