import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../service/AuthService';
import { LelangService } from '../../service/LelangService';
import { Item } from '../../models/item';
import firebase from 'firebase';

/**
 * Generated class for the ViewProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-profile',
  templateUrl: 'view-profile.html',
})
export class ViewProfilePage {
  viewProfile: string;
  userId: any;
  userName: any;
  image: any;
  telp: any;
  email: any;
  lelang: Item[];
  lelangTemp = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public authSvc: AuthService, public lelangSvc: LelangService) {
    this.viewProfile = "lelang";
    this.userId = navParams.get('user');
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewProfilePage');
  }

  ionViewWillEnter() {
    this.lelangTemp = [];
    this.authSvc.getUserById(this.userId).on('value', personSnapshot => {
      this.userName = personSnapshot.val().fullName;
      this.image = personSnapshot.val().imageUrl;
      this.email = personSnapshot.val().email;
      this.telp = personSnapshot.val().telp;
      
      this.lelang = this.objectToArray(personSnapshot.val().startLelang);

      Object.keys(this.lelang).forEach( key => {
        var lelangItem = this.lelangSvc.getLelangById(this.lelang[key].kode);
        lelangItem.on('value', personSnapshot => {
          this.lelangTemp.push(personSnapshot.val());
        });
      });
    });
  }

  objectToArray(obj) {
    var arr = [];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };

}
