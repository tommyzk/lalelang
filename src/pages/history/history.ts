import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Item } from '../../models/item';
import { LelangService } from '../../service/LelangService';
import { AuthService } from '../../service/AuthService';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  default: string;
  currentItems: Item[];
  historyLelang: Item[];
  detailItem = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public lelangSvc: LelangService,
    public authSvc: AuthService) {
    this.default = "bid"
    var lelanganRef = lelangSvc.getHistoryBid()
    var tempLelang = lelangSvc.getHistoryLelang()
    
    lelanganRef.on('value', personSnapshot => {
      this.currentItems = this.objectToArray(personSnapshot.val());
    });
    
    tempLelang.on('value', personSnapshot => {
      this.historyLelang = this.objectToArray(personSnapshot.val());
    });

    Object.keys(this.currentItems).forEach(key=> {
      var detailBarang = lelangSvc.getLelangById(this.currentItems[key].kodeBarang)
      detailBarang.on('value', personSnapshot => {
        
        var detailItem = {
          namaBarang: personSnapshot.val().namaBarang,
          nominalBid: this.currentItems[key].nominalBid,
          jamBid: this.currentItems[key].jamBid,
          tanggalBid: this.currentItems[key].tanggalBid
        }
        this.detailItem.push(detailItem)
      });
    });
  }
  
  objectToArray(obj) {
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };

  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad HistoryPage');
  }

}
