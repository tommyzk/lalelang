import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, ToastController, Loading } from 'ionic-angular';
import { AuthService } from '../../service/AuthService';
import { TutorialPage } from '../tutorial/tutorial';
import { WelcomePage } from '../welcome/welcome';

/**
 * Generated class for the VerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {

  public loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authSvc: AuthService,
    private app: App,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPage');
  }

  logout(){
    this.authSvc.logout().then(()=>{
      console.log("loged out")
    })
    .catch((error) => {
        console.error(error)
    });
    //this.navCtrl.popToRoot();
    //this.navCtrl.setRoot(WelcomePage, {}, {animate: true, direction: 'back'});
    this.app.getRootNav().setRoot(WelcomePage, {animate: true, direction: 'back'});
    
  }

  resent(){
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.authSvc.resentVerEmail().then(
      (result) => {
        this.loading.dismiss().then( () => {
          
          let toast = this.toastCtrl.create({
            message: "Send",
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        })
      })
      .catch((error) => {
        this.loading.dismiss().then( () => {
        
          let toast = this.toastCtrl.create({
            message: "Failed",
            duration: 3000,
            position: 'top'
          });
          toast.present();
          console.error(error)
        })
      });
  }

}
