import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Item } from '../../models/item';
import { Items } from '../../providers/providers';
import { LelangService } from '../../service/LelangService';
import { AuthService } from '../../service/AuthService';
import * as moment from 'moment';
import { Observable } from 'rxjs/Rx';


/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
  //config:{mode: "md"},
})
export class FavoritePage {
  default: string;
  currentItems: Item[];
  detailItems = [];
  tick: any;
  add: any;
  date: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public items: Items, public lelangSvc: LelangService,
    public authSvc: AuthService) {
    
  }
  
  objectToArray(obj) {
    var arr =[];
    for(let o in obj) {
      if (obj.hasOwnProperty(o)) {
        arr.push(obj[o]);
      }
    }
    return arr;
  };

  ionViewDidLoad() {
    //console.log('ionViewDidLoad FavoritePage');
  }

  ionViewWillEnter(){
    var lelanganRef = this.lelangSvc.getFavorites()
    
    lelanganRef.on('value', personSnapshot => {
      this.detailItems = []
      this.currentItems = this.objectToArray(personSnapshot.val());
      
      Object.keys(this.currentItems).forEach(key=> {
        var detailBarang = this.lelangSvc.getLelangById(this.currentItems[key].kode)
        detailBarang.on('value', personSnapshot => {
          
          var detailItem = {
            namaBarang: personSnapshot.val().namaBarang,
            imageUrl: personSnapshot.val().imageUrl,
            pelelang: personSnapshot.val().pelelang,
            kode: personSnapshot.val().kode,
            tanggalSelesai: personSnapshot.val().tanggalSelesai
            //sisaWaktu: this.tick
          }
          this.detailItems.push(detailItem)
        })
      })
    })
  }

  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

}
