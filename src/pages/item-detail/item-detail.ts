import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, PopoverController, Slides, ToastController } from 'ionic-angular';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Observable } from 'rxjs/Rx';
import * as moment from 'moment';
import { Items } from '../../providers/providers';
import { LelangService } from '../../service/LelangService';
import { AuthService } from '../../service/AuthService';


@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {
  @ViewChild(Slides) slides: Slides;
  item: any;
  tick: any;
  date: any;
  add: any;
  show: any;
  user: any;
  fullName: any;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public items: Items,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public lelangSvc: LelangService,
    public authSvc: AuthService,
    public toastCtrl: ToastController
  ) {
    //this.item = navParams.get('item') || items.defaultItem;

    var itemTemp = navParams.get('item');
    var itemId = itemTemp.kode;
    var temp = lelangSvc.getLelangById(itemId);
    temp.on('value', personSnapshot => {
      this.item = personSnapshot.val()
    });

    var authId = this.item.pelelang
    var temp = authSvc.getUserById(this.item.pelelang)
    temp.once('value', personSnapshot => {
      this.fullName = personSnapshot.val().fullName
    });

    this.add = moment(this.item.tanggalSelesai+" "+this.item.jamLelang, "YYYY-MM-DD hh:mm:ss");
    
    Observable.interval(1000).subscribe( 
      t => {
        this.date = moment();
        this.tick = this.add - this.date;
      }
    );
  }

  confirmFavorite(item: any) {
    let alert = this.alertCtrl.create({
      title: 'Favorite',
      message: 'Do you want add this to your favorites?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.favorite(item);
          }
        }
      ]
    });
    alert.present();
  }

  favorite(item: any){
    this.lelangSvc.favorite(item.kode, item.pelelang)
      .then( () => {
        let toast = this.toastCtrl.create({
          message: "Success add favorite",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        })
      
  }

  bid() {
    let bidModal = this.modalCtrl.create('ModalItemDetailPage', {
      item: this.item
    });
    bidModal.present();
  }

  share(myEvent) {
    let popover = this.popoverCtrl.create('PopoverShareSocmedPage');
    popover.present({
      ev: myEvent
    });
  }

  viewProfile() {
    this.navCtrl.push('ViewProfilePage', { user: this.item.pelelang });
  }
}
